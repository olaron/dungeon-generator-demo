from room import RoomGenerator, Constraints

if __name__ == "__main__":
    generator = RoomGenerator("room.ans")
    generator.ground()

    c = Constraints()

    # The player can go from west to east, or east to west
    c.traversable(start_dir="w", end_dir="e", possible=True)
    c.traversable(start_dir="e", end_dir="w", possible=True)

    # north to south, or south to north
    c.traversable(start_dir="n", end_dir="s", possible=True)
    c.traversable(start_dir="s", end_dir="n", possible=True)

    # But not in any other combinaison
    c.traversable(start_dir="e", end_dir="n", possible=False)
    c.traversable(start_dir="e", end_dir="s", possible=False)
    c.traversable(start_dir="w", end_dir="n", possible=False)
    c.traversable(start_dir="w", end_dir="s", possible=False)
    c.traversable(start_dir="n", end_dir="e", possible=False)
    c.traversable(start_dir="s", end_dir="e", possible=False)
    c.traversable(start_dir="n", end_dir="w", possible=False)
    c.traversable(start_dir="s", end_dir="w", possible=False)

    c.no_locks()
    c.no_keys()

    solver = generator.solve(c.get())
    t = 0
    while not solver.wait(1):
        t += 1
        print(t)
    room = solver.get_room()
    room.print()
