# Requirements

Anaconda or Miniconda with Python 3

# Create a new environment with clingo installed

```bash
conda create --name dungeon clingo
```

# Run the demo

```bash
conda activate dungeon
python main.py
```

Contraints on the generated room can be changed in main.py

# Tiles

'`.`' are ground tiles. The player can walk on them.

'`#`' are wall tiles. The player cannot walk on them.

'`s`' are switches. The player cannot walk on them. They can be activated by the player from one of the 8 tiles around it. Doing so will change which doors (red or blue) are open.

'`b`' are blue doors. The player can walk on them if blue doors are open and red doors are closed, otherwise they act as walls. 

'`d`' are red doors. The player can walk on them if red doors are open and blue doors are closed.

'`l`' are locked doors. The player can consume a key in order to go through it. (experimental)

# Rules

The player can move in 4 directions (north, south, east or west) but not diagonally.

If the player is on an open door while activating a switch, the player will be on top of a closed door, and can walk on other closed doors until the player gets back to the ground (or an open door).
